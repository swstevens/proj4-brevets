# ACP Time Calculator

An Implemented RUSA ACP controle time calculator using flask and ajax.

Base code provided by Michal Young, added upon by Shea Stevens.

## Overview

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).  

This application is an automatically updating version of (https://rusa.org/octime_acp.html). 

## Rules

There are a variety of rules that the calculator adheres to. They are as follows:

1 - Times are calculated using the below timetable. The open times are calculated using the maximum speed, while the close times are calculated by using the minimum speed.

control location (km)   minimum speed (km/hr)  	maximum speed (km/hr)

0-200:               	15,              		34

200-400:             	15,              		32

400-600:            	15,              		30

600-1000:            	11.428,          		28

1000-1300:           	13.333,          		26

2 - The close time for the end checkpoint for each brevit is predertermined and does not follow the calculations mentioned above. They are as listed below.

Brevet Distance (km)	Close Time (hours past start)

200:					13.5

300:					20

400:					27

600:					40

1000:					75

The website does not check for these rules:

1. Checkpoints should be more or less regularly spaced between the brevet start and finish. According to the distance, there should be 2 to 4 checkpoints for a 200 KM brevet, 3 to 5 checkpoints for a 300 KM brevet, 4 to 6 checkpoints for a 400 brevet, 5 to 7 checkpoints for a 600 KM brevet, and 6 to 10 checkpoints for a 1000 KM brevet.
2. Checkpoints must not exceed 1.2 times the full length of the brevet.

## Other Notes

The acp_times function, which maintains the calculations above, is easily editable to add more control location speed requirements as well as specific brevet distance close times. When looking at the file, find CONTROLS for rule 1, and DISTANCES for rule 2. By adding new entries, you can update the applications to your own needs.