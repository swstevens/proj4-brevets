"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    #distance = flask.request.form("distance")
    #begin_date = flask.request.form("begin_date")
    #begin_time = flask.request.form("begin_time")

    app.logger.debug("Got a JSON request")
    km = request.args.get('km', type=float)
    date = request.args.get('dates', type=str)
    time = request.args.get('time', type=str)
    distance = request.args.get('distance', type=int)    

    print(date)
    print(time)
    print(distance)
    # print('\n')

    datetime = arrow.get(date+time, "YYYY-MM-DDHH:mm")

    print(datetime.isoformat())
    print('\n')


    print("km: " +str(km))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, distance, datetime)
    close_time = acp_times.close_time(km, distance, datetime)
    print(open_time)
    print(close_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
