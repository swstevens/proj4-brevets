from acp_times import *
import arrow

"""
I was having trouble with my Ubuntu WSL system not recognizing the installed arrow package
so I was unable to run nosetests as nosetests

I instead manually tested each assert using acp_times and a separate python file.
Sorry if the nosetest does not run properly
"""

def test_zero():
    test_time = arrow.get("2020-10-10 12:00:00", "YYYY-MM-DD HH:mm:ss")
    assert open_time(0, 200, test_time) == test_time
    assert close_time(0, 400, test_time) == test_time.shift(hours=+1).isoformat()
    assert close_time(0, 600, test_time) == arrow.get("2020-10-10 13:00:00", "YYYY-MM-DD HH:mm:ss")

def test_200():
    start_time = arrow.get("2020-10-10 17:53:00", "YYYY-MM-DD HH:mm:ss")
    finish_time = arrow.get("2020-10-11 01:20:00", "YYYY-MM-DD HH:mm:ss")
    finish_time_a = arrow.get("2020-10-11 01:30:00", "YYYY-MM-DD HH:mm:ss")
    test_time = arrow.get("2020-10-10 12:00:00", "YYYY-MM-DD HH:mm:ss")
    # the results for the 200 km control point should be the same
    # regardless of total brevet distance
    assert open_time(200, 200, test_time) == start_time.isoformat()
    assert open_time(200, 400, test_time) == start_time.isoformat()
    assert open_time(200, 600, test_time) == start_time.isoformat()
    assert open_time(200, 1000, test_time) == start_time.isoformat()

    assert close_time(200, 200, test_time) == finish_time_a.isoformat()
    assert close_time(200, 400, test_time) == finish_time.isoformat()
    assert close_time(200, 600, test_time) == finish_time.isoformat()
    assert close_time(200, 1000, test_time) == finish_time.isoformat()

def test_exact_ends():
    start_time = arrow.get("2020-10-10 12:00:00", "YYYY-MM-DD HH:mm:ss")
    time_200 = start_time.shift(hours=+13.5)
    time_300 = start_time.shift(hours=+20)
    time_400 = start_time.shift(hours=+27)
    time_600 = start_time.shift(hours=+40)
    time_1000 = start_time.shift(hours=+75)

    assert close_time(200,200,start_time) == time_200.isoformat()
    assert close_time(300,300,start_time) == time_300.isoformat()
    assert close_time(400,400,start_time) == time_400.isoformat()
    assert close_time(600,600,start_time) == time_600.isoformat()
    assert close_time(1000,1000,start_time) == time_1000.isoformat()


def test_over_bounds():
    test_time = arrow.get("2020-10-10 12:00:00", "YYYY-MM-DD HH:mm:ss")
    # as per the ruleset, control points past the toal brevet dist
    # are treated as if they are at the max brevet dist
    assert open_time(1000,1000, test_time) == open_time(1200,1000,test_time)
    assert close_time(1000,1000, test_time) == close_time(1200,1000,test_time)
